package praktikum10;

import java.util.ArrayList;

import lib.TextIO;

public class Tulpdiagramm {
	public static void main(String[] args) {
		ArrayList<Integer> numbrid = new ArrayList<Integer>(); 
		while(true){
			System.out.println("ütle number");
			int sisestus = TextIO.getlnInt();
			if( sisestus == 0){
				break;
			}else if( sisestus < 0){
				System.out.println("number peab olema positiivne");
			}else{
				numbrid.add(sisestus);
			}
		}
		int h = Integer.MIN_VALUE;
		for(int i = 0; i < numbrid.size(); i++){
			if(h < numbrid.get(i)){
				h = numbrid.get(i);
			}
		}
		double kordaja = 10.0 / h;
		for(int i = 0; i < numbrid.size(); i++){
			int number = numbrid.get(i);
			System.out.printf("%2d %s\n", number , genereerixsid(number,kordaja));
		}		
	}
	public static String genereerixsid(int nx, double kordaja){
		String xsid = "";
		for(int i = 0; i < nx*kordaja;i++){
			xsid = xsid + "x";
		}
		return xsid;
	}

}
