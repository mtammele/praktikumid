package praktikum15;

public class Silinder extends Ring {
	Punkt p1;
	double raadius;
	double k6rgus;

	public Silinder(Ring ring, double k) {
		super(ring.keskpunkt, ring.raadius);
		p1 = ring.keskpunkt;
		raadius = ring.raadius;
		k6rgus = k;

	}
	
	public double spindala(){
		double SP = 2 * pindala(); 
		double KP = k6rgus * ymberm66t();
		double S = SP + KP;
		
		return S;
		
	}
	public double ruumala(){
		double v = pindala() * k6rgus;
		return v;
	}
	
}
