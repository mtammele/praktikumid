package praktikum15;

public class Ring {
	
	Punkt keskpunkt;
	double raadius;
	
	public Ring(Punkt p, double r){
		keskpunkt = p;
		raadius = r;
		
	}
	public double ymberm66t(){
		double P = Math.PI * 2*raadius;
		
		return P;
	}
	public double pindala(){
		double S = Math.PI * Math.pow(raadius, 2);
		return S;
	}
	

}
