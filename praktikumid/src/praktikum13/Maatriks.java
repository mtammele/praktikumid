package praktikum13;

public class Maatriks{
	public static void main(String[] args) {
		int[] test = new int[]{1,6,5,6};
		int[][] test1 = {
				{1, 1, 1, 1, 1},
				{2, 3, 4, 5, 6},
				{3, 4, 5, 6, 7},
				{4, 0, 6, 7, 8},
				{5, 6, 7, 8, 9},
		};
		//System.out.println(korvalDiagonaaliSumma(test1));
		//System.out.println(korvalDiagonaaliSummaEffektiivsem(test1));
		int[] est2 = ridadeMaksimumid(test1);
		//tryki(est2);
		//System.out.println(miinimum(test1));
		//tryki(kahegaJaakMaatriks(3,5));
		int[][] transponeeritav = {
				{1, 2},
				{3, 4},
				{5, 6},
		};
		int[][] transponeeritud = transponeeri(test1);
		tryki(transponeeritud);
	}
	
	public static void tryki(int[] massiiv){
		for(int i = 0 ;i<massiiv.length;i++){
			System.out.print(massiiv[i]+" ");
		}
		
	}
	public static void tryki(int[][] maatriks){
		for(int i = 0;i<maatriks.length;i++){
			for(int j = 0;j<maatriks[i].length;j++){
				System.out.print(maatriks[i][j]+" ");
			}
			System.out.println();
		}
		
	}
	public static int[] ridadeSummad(int[][] maatriks){
		int[] summad = new int[maatriks.length];
		
		for(int i = 0;i<maatriks.length;i++){
			for(int j = 0;j<maatriks[i].length;i++){
				summad[i] = maatriks[i][j] + summad[i]; 
			}
		}
		return summad;
	}
	public static int korvalDiagonaaliSumma(int[][] maatriks){
		int summa = 0;
		for(int i = 0;i<maatriks.length;i++){
			for(int j = 0 ;j<maatriks[i].length;j++){
				if( j == (maatriks[i].length-1-i) ){ summa = summa +maatriks[i][j];}
			}
		}
		
		return summa;
	}
	public static int korvalDiagonaaliSummaEffektiivsem(int[][] maatriks){
		int summa = 0;
		for(int i = 0;i<maatriks.length;i++){
			summa += maatriks[i][maatriks.length-i-1];
		}
		
		return summa;
	}
	public static int[] ridadeMaksimumid(int[][] maatriks){
		int[] maksimumid = new int[maatriks.length];
		
		for(int i = 0 ;i<maatriks.length;i++){
			int max = Integer.MIN_VALUE;
			for( int j = 0;j<maatriks[i].length;j++){
				if(max < maatriks[i][j]){ max = maatriks[i][j];}
			}
			maksimumid[i] = max;
		}
		
		return maksimumid;
	}
	public static int miinimum(int[][] maatriks){
		int min = Integer.MAX_VALUE;
		for(int i = 0;i<maatriks.length;i++){
			for(int j = 0;j<maatriks[i].length;j++){
				if(min > maatriks[i][j]){ min = maatriks[i][j];}
			}
		}
		return min;
	}
	public static int[][] kahegaJaakMaatriks(int ridu, int veerge){
		int[][] maatriks = new int[ridu][veerge];
		for(int i = 0;i<maatriks.length;i++){
			for(int j = 0; j<maatriks[i].length;j++){
				maatriks[i][j] = (i+j)%2;
			}
		}
		return maatriks;
	}
	public static int[][] transponeeri(int[][] maatriks){
		
		int[][] ku = new int[maatriks[0].length][maatriks.length];
		for(int i = 0;i<ku.length;i++){
			for(int j = 0;j<ku[i].length;j++){
				ku[i][j] = maatriks[j][i];
			}
		}
		return ku;
	}

}
