package praktikum6;

import lib.TextIO;

public class Meetodid {
	public static void main(String[] args) {
		
		System.out.println("Sisesta arv");
		int a = TextIO.getlnInt();
		int akuup = kuup(a);
		
		System.out.println("Arvu kuup on " + akuup);
	}
	public static int kuup(int arv){
		return (int) Math.pow(arv, 3);
	}
}
