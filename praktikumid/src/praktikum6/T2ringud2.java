package praktikum6;

public class T2ringud2 {
	public static void main(String[] args) {
		int number;   // The number to be printed.
		number = 1;   // Start with 1.
		int sum = 0;
		while ( number < 6 ) {  // Keep going as long as number is < 6.
			int roll = (int) (Math.random() * 6 +1);
			System.out.println("T2ring veeretas " + roll);
			sum = sum + roll; 
		   // System.out.println(number + " ja " + sum);
		    number = number + 1;  // Go on to the next number.
		}
		System.out.println("Veeretasite kokku " + sum);
	}

}
