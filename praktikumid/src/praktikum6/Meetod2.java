package praktikum6;

import lib.TextIO;

public class Meetod2 {
	
//	public static int kasutajaSisestus(int min, int max){
//		System.out.println("Sisesta arv vahemikus " + min + " kuni " + max);
//		
//	}
	public static int kasutajaSisestus(int min, int max){
		
		while(true) {
			System.out.println("Sisesta arv vahemikus " + min + " kuni " + max);
		
			int sisestus = TextIO.getlnInt();
			
			if( sisestus >= min && sisestus <= max){
				return sisestus;
			}
			TextIO.putln("Ei ole vahemikus " + min + ";" + max);
		
		}
	}
	public static void main(String[] args) {
		int arv = kasutajaSisestus(1, 5);
		System.out.println("Kasutaja sisestas " + arv);
		
	}
}
