package praktikum5;

public class Tsykksel {
	public static void main(String[] args) {
		if(true){
			System.out.println("Tingimus vastab tõele");
		}
		
		int j = 0;
		while (j < 3) {
			System.out.println("tingimus vastab tõele (while)");
			j++; // i= i + 1;
		}
		
		for (int i = 0; i < 10; i++){
			System.out.println("for tsykkel, i: " + i);
		}
	}

}
