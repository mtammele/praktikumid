package praktikum5;

public class Tsykkel5 {
	
public static void main(String[] args) {
		
		int tabelisuurus = 5;
		
		for (int i = 0; i < tabelisuurus; i++){
			for (int j = 0; j < tabelisuurus; j++){
				if (j == 0 && i == 0){
					System.out.print("1 ");
				} else{
					System.out.print("0 ");
				}
				System.out.print("(i:" + i + " j:" + j + ") ");
			}
			System.out.println();  // reavahetus
		}
	}

}
