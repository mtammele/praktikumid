package praktikum5;

public class Tsykkel7 {
	public static void main(String[] args) {
		
		int tabelisuurus = 10;
		
		for (int i = 0; i <= tabelisuurus; i++){
			for (int j = 0; j <= tabelisuurus; j++){
				if ( i == 0 || i == tabelisuurus ){
					System.out.print("- ");
				}
				else if( j == 0 || j == tabelisuurus){
						System.out.print("| ");
				} 
				else if( i == j){
					System.out.print("x ");
				
				}
				else if( i + j == tabelisuurus){
					System.out.print("x ");
				}
				else{
					System.out.print("0 ");
				}
				//System.out.print("(i:" + i + " j:" + j + ") ");
			}
			System.out.println();  // reavahetus
		}
	}

}
