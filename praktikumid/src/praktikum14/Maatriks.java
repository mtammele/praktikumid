package praktikum14;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

public class Maatriks {
	public static void main(String[] args) {
		
		String kataloogitee = Tekstifail.class.getResource(".").getPath();
		File tekst = new File(kataloogitee + "kala.txt");
		
		ArrayList<Integer> r = sisu(tekst);
		
		for(int re : r ){
			System.out.println(re);
			
		}
		
	}

	
	
	public static ArrayList<Integer> sisu(File tekst){
		ArrayList<Integer> r = new ArrayList<Integer>();
		try {
			BufferedReader t = new BufferedReader(new FileReader(tekst));
			String rida;
			
			while((rida = t.readLine()) != null){
				if(onNumber(rida)){ 
					r.add(Integer.parseInt(rida));
				}
			}
			t.close();
		}catch (FileNotFoundException e) {
			System.out.println(e);
		}catch(Exception g){
			System.out.println(g);
		}
		
		return r;
	}
	
	public static boolean onNumber(String r){
		try{
			Integer.parseInt(r);
			return true;
		}catch(NumberFormatException e){
			return false;
		}
		
	}
}
