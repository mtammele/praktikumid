package praktikum14;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

public class Numbrd {
	public static void main(String[] args) {
		String kataloogitee = Tekstifail.class.getResource(".").getPath();
		File tekst = new File(kataloogitee + "kala.txt");
		
		ArrayList<Double> r = sisu(tekst);
		double kokk = 0;
		int h = 0;
		for(Double re : r ){
			kokk = kokk + re;
			h++;
		}
		System.out.println(kokk/h);
	}
	
	
	
	
	public static ArrayList<Double> sisu(File tekst){
		ArrayList<Double> r = new ArrayList<Double>();
		try {
			BufferedReader t = new BufferedReader(new FileReader(tekst));
			String rida;
			while((rida = t.readLine()) != null){
				if(onNumber(rida)){ 
					r.add(Double.parseDouble(rida));
				}
			}
			t.close();
		}catch (FileNotFoundException e) {
			System.out.println(e);
		}catch(Exception g){
			System.out.println(g);
		}
		
		return r;
	}
	public static boolean onNumber(String r){
		try{
			Double.parseDouble(r);
			return true;
		}catch(NumberFormatException e){
			return false;
		}
		
	}

}
