package praktikum14;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;

public class Tekstifail {
	
	public static void main(String[] args) {
		
		String kataloogitee = Tekstifail.class.getResource(".").getPath();
		File tekst = new File(kataloogitee + "kala.txt");
		ArrayList<String> r = new ArrayList<String>(Loeb(tekst));
		Collections.sort(r);
		for(String rida : r){
			System.out.println(rida);
		}
		
	}
	
	public static ArrayList<String> Loeb( File tekst){
		ArrayList<String> r = new ArrayList<String>();
		try {
			BufferedReader t = new BufferedReader(new FileReader(tekst));
			String rida;
			while((rida = t.readLine()) != null){
				r.add(rida);
			}
			t.close();
		}catch (FileNotFoundException e) {
			System.out.println(e);
		}catch(Exception g){
			System.out.println(g);
		}
		return r;
	}
	

}
