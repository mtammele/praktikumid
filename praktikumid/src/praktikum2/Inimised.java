package praktikum2;

import lib.TextIO;

public class Inimised {
	public static void main(String[] args) {
		int inimesed;
		int grupp;
		int mitugruppi;
		int jääk;

		System.out.println("Palju on inimesi?");
		inimesed = TextIO.getInt();
		System.out.println("Kui suured on grupid?");
		grupp = TextIO.getInt();

		mitugruppi = inimesed / grupp;
		//jääk = inimesed - (mitugruppi * grupp);
		jääk = inimesed % grupp;
		

		System.out.println("Tekib " + mitugruppi + " gruppi ja ülejääb " + jääk + " inimest");

	}

}
