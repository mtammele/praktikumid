package praktikum12;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class Tiivik extends Applet{
	
	@Override
	public void paint(Graphics g) {
		 int w = getWidth();
	     int h = getHeight();
	     g.setColor(Color.white);
	     g.fillRect(0, 0, w, h);
	     g.setColor(Color.black);
	     int keskkohtX = w/2;
	     int keskkohtY = h/2;
	     int raadius = 200;
	     double nurk;
	     int r = 7;
	     double e = Math.PI/r;
	     for( nurk = -Math.PI; nurk < Math.PI;nurk = nurk + Math.PI/(r/2)){
	    	 int x = (int)(raadius * Math.cos(nurk+e)+keskkohtX);
	    	 int y = (int)(raadius * Math.sin(nurk+e)+keskkohtY);
	    	 int x1 = (int)(raadius * Math.cos(nurk)+keskkohtX);
	    	 int y1 = (int)(raadius * Math.sin(nurk)+keskkohtY);
	    	 g.drawLine(keskkohtX, keskkohtY, x, y);
	    	 g.drawLine(keskkohtX, keskkohtY, x1, y1);
	    	 g.drawLine(x, y, x1, y1);
	     }
		
	}

}
