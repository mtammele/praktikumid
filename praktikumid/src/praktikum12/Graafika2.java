package praktikum12;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class Graafika2 extends Applet{
	@Override
	public void paint(Graphics g) {
		
		int h = getHeight();
		int w = getWidth();
		
		for( int y = 0; y < h; y++){
			g.drawLine(0, y, w, y);
			double e = (double) y/h;
			//red, green,blue ; 0...255
			Color color = new Color ((int)(255*e),255,(int)(255*e));
			g.setColor(color);
		}
	}
}
