package praktikum12;

import java.awt.Color;
import java.awt.Graphics;
import java.applet.Applet;

/**
 * Ringjoone valemi jĆ¤rgi ringi joonistamise nĆ¤ide
 * @author Mikk Mangus
 */
@SuppressWarnings("serial")
public class Spiraal extends Applet {

    private Graphics g;
    
    public void paint(Graphics g) {
        this.g = g;
        joonistaTaust();
        joonistaRing();
    }
    
    /**
     * Katab tausta valgega
     */
    public void joonistaTaust() {
        int w = getWidth();
        int h = getHeight();
        g.setColor(Color.white);
        g.fillRect(0, 0, w, h);
    }
    
    /**
     * Joonistab ringi
     */
    public void joonistaRing() {
        g.setColor(Color.black);
        int keskkohtX = getWidth() / 2;
        int keskkohtY = getHeight() / 2;
        //int raadius = 50;
        int lastx = 0;
        int lasty = 0;
        
        for (double nurk = 0; nurk <= Math.PI * 20; nurk = nurk + .001) {
        	double raadius = 1 +nurk *3;
            int x = (int) (raadius * Math.cos(nurk) +keskkohtX);
            int y = (int) (raadius * Math.sin(nurk) +keskkohtY);
            //g.fillRect(keskkohtX + x, keskkohtY + y, 2, 2);
            if(lastx != 0){
            	g.drawLine(x, y, lastx, lasty);
            }
            lastx = x;
            lasty = y;
        }
    }
}


