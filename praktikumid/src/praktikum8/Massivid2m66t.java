package praktikum8;

public class Massivid2m66t {
	
	public static void main(String[] args) {
		int[] t= {298,2,7,98,4,8,1,9,194};
		int k = maksimum(t);
		System.out.println("max on :" + k);
		
		int[][] kaks = { 	{  1,  0, 12, -1 },
                			{  7, -3,  2,  5 },
                			{ -5, -2,  2, 28 }
             			};
		int ji = maksimumM(kaks);
		System.out.println("maatriks :" + ji);
		//System.out.println(kaks.length);
		//System.out.println(kaks[1].length);
		
	}
	
	
	// meetod, mis leiab ühemõõtmelise massiivi maksimumi
	public static int maksimum(int[] massiiv){
		int g = 0;
		for(int i = 0; massiiv.length > i; i++){
			if( massiiv[i] > g){
				g = massiiv[i];
			}
			
		}
		return g;
	}

	// meetod, mis leiab maatriksi maksimumi
	public static int maksimumM(int[][] maatriks) {
		int h = 0;
		
		for(int i = 0; i < maatriks.length; i++){
			for(int j = 0; j < maatriks[0].length; j++){
				if(maatriks[i][j] > h){
					h = maatriks[i][j];
				}
			}
		}
			
		
		return h;
		// maatriksi rea maksimumi leidmiseks saad siin edukalt kasutada eelmist meetodit
	}

}
