package kodu;

public class Reaalarvud {
	
	public static void main(String[] args) {
		double[] arvud = {5,1,6,29};
		System.out.println(allaKeskmise(arvud));
	}
	
	public static int allaKeskmise (double[] d){
		double g = 0;
		for(int i = 0; i < d.length;i++){
			g = g+d[i];
			
		}
		double kesk = g/d.length;
		int kokku = 0;
		for(int i = 0; i < d.length;i++){
			if( d[i] < kesk){
				kokku = kokku+1;
			}
			
		}
		return kokku;
	}
	
}
