package praktikum7;

import lib.TextIO;

public class Kulljakiripanustega {
	 public static void main(String[] args) {
		int kasutajaRaha = 25;
				
		while(true){
			
			
			System.out.print("Teil on " + kasutajaRaha + " raha. ");
			
			int panus = kasutajaSisestus("Palju panustate (max 25)", 1, 25);
			int kasutajaArvab = kullvkiri("Kull(0) v6i Kiri(1)?",0 , 1);
			int vise = suvaArv();
			
			if( kasutajaRaha > 0){
				System.out.println("Teie arvasite " + kasutajaArvab + "; Visati " + vise);
				
				if( kasutajaArvab == vise){
					System.out.println("Te v6itsite");
					kasutajaRaha = (kasutajaRaha + panus);
				}else{
					System.out.println("Te kaotasite");
					kasutajaRaha = (kasutajaRaha - panus);
					if( kasutajaRaha <= 0){
						System.out.println("Teil sai raha otsa");
						break;
					}
				}
			}
		}
	 }
	 
	 public static int kullvkiri(String kysimus, int min, int max){
		 System.out.println(kysimus);
		 while(true){
			 int a = TextIO.getlnInt();
		 	 if ( a >= min && a <= max){
		 		 return a;
		 	 }
		 	 System.out.println("Sisesta " + min + " v6i " + max );
		 }
		 
	 }
	 
	 public static int suvaArv(){
		 
		 int arv = (Math.random() > 0.5 ? 0:1);
		 return arv;
	 }
	 public static int kasutajaSisestus(String kysimus, int min, int max){
		 System.out.println(kysimus);
		 while(true){
			int a = TextIO.getlnInt();
		 	if ( a >= min && a <= max){
		 		return a;
		 	}System.out.println("Minimaalne panus on 1 ja maximaalne on 25");
			 
		 }
	 }
	 

}
