package praktikum7;

import lib.TextIO;

public class Tagurpidis6na {
	public static void main(String[] args) {
		
		String s6na = tagurpidiS6na();
		
		System.out.println("Tagurpidi see s�na on " + s6na);
		
	}
	
	public static String tagurpidiS6na( ){
		
		System.out.println("�tle mingi s6na");
		String a = TextIO.getlnString();
		
		char[] t = a.toCharArray();
		int algus = 0;
		int l6pp = t.length-1;
		char temp;
		
		while(algus < l6pp){
			temp = t[algus];
			t[algus] = t[l6pp];
			t[l6pp] = temp;
			algus ++;
			l6pp --;	
		}
	    return new String(t);
		
		
		
	}

}
