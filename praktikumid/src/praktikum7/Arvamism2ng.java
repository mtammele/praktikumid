package praktikum7;

import lib.TextIO;

public class Arvamism2ng {
	public static void main(String[] args) {
		
		System.out.println("Arvuti mõtles arvu 1-100, mis arvu ta mõtles?");
		
		
		int arvutiArv = arvamus(1, 100);
		
		
		while(true){
			int kasutajaArv = kasutajaSisestus(1,100);
			if (arvutiArv == kasutajaArv){
				System.out.println("Õigesti arvasite");
				break;
			}else if( arvutiArv < kasutajaArv){
				System.out.println("Arvuti mõeldud arv on väiksem");
			}else if( arvutiArv > kasutajaArv){
				System.out.println("Arvuti mõeldud arv on suurem");
			}
		}
			
		
		
		
		
	}
	public static int kasutajaSisestus(int min, int max){
		while(true){
			int a = TextIO.getlnInt();
			if( a <= max && a >= min){
				return a;
			}
			System.out.println("Arv peab olema vahemikus 1-100");
		}
	}
	public static int arvamus(int min, int max){
		
		int vahemik = ( max - min + 1);
		int arv = (int) (Math.random() * vahemik);
		return arv;
		
	}
	

}
